exports.up = function (knex) {
    return knex.schema.createTable('deliveries', table => {
        table.increments('id').primary()
        table.string('desc').notNull()
        table.string('price').notNull()
        table.string('quantity').notNull()
        table.string('doneAt')
        table.integer('userId').references('id').inTable('users').notNull()
    })
}

exports.down = function (knex) {
    return knex.schema.dropTableIfExists('deliveries')
}
