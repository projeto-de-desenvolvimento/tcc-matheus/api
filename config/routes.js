module.exports = app => {
    app.post('/signup', app.api.user.save)
    app.post('/signin', app.api.auth.signin)

    app.route('/user/name')
        .all(app.config.passport.authenticate())
        .get(app.api.user.getUser)

    app.route('/deliveries')
        .all(app.config.passport.authenticate())
        .get(app.api.deliverie.getDeliveries)
        .post(app.api.deliverie.save)

    app.route('/deliveries/:id')
        .all(app.config.passport.authenticate())
        .delete(app.api.deliverie.remove)

    app.route('/deliveries/:id/toggle')
        .all(app.config.passport.authenticate())
        .put(app.api.deliverie.toggleDeliverie)

    app.route('/employees')
        .all(app.config.passport.authenticate())
        .get(app.api.func.getEmployees)
        .post(app.api.func.save)

    app.route('/employees/:id')
        .all(app.config.passport.authenticate())
        .delete(app.api.func.remove)

    app.route('/employees/:id/toggle')
        .all(app.config.passport.authenticate())
        .put(app.api.func.toggleEmployees)

    app.route('/pluviometers')
        .all(app.config.passport.authenticate())
        .get(app.api.pluviometer.getPluviometers)
        .post(app.api.pluviometer.save)

    app.route('/pluviometers/:id/toggle')
        .all(app.config.passport.authenticate())
        .put(app.api.pluviometer.togglePluviometer)

    app.route('/maintenances')
        .all(app.config.passport.authenticate())
        .get(app.api.maintenance.getMaintenance)
        .post(app.api.maintenance.save)

    app.route('/maintenances/:id')
        .all(app.config.passport.authenticate())
        .delete(app.api.maintenance.remove)

    app.route('/maintenances/:id/toggle')
        .all(app.config.passport.authenticate())
        .put(app.api.maintenance.toggleMaintenance)

}