const moment = require('moment')

module.exports = app => {
    const getEmployees = (req, res) => {

        app.db('employees')
            .where({ userId: req.user.id })
            .then(employees => res.json(employees))
            .catch(err => res.status(500).json(err))
    }

    const save = (req, res) => {

        req.body.userId = req.user.id

        app.db('employees')
            .insert(req.body)
            .then(_ => res.status(204).send())
            .catch(err => res.status(400).json(err))
    }

    const remove = (req, res) => {
        app.db('employees')
            .where({ id: req.params.id, userId: req.user.id })
            .del()
            .then(rowsDeleted => {
                if (rowsDeleted > 0) {
                    res.status(204).send()
                } else {
                    const msg = `Não foi encontrada usuário com id ${req.params.id}`
                    res.status(400).send(msg)
                }
            })
    }

    const updateEmployeesDoneAt = (req, res, doneAt) => {
        app.db('employees')
            .where({ id: req.params.id, userId: req.user.id })
            .update({ doneAt })
            .then(_ => res.status(204).send())
            .cath(err => res.status(400).json(err))
    }

    const toggleEmployees = (req, res) => {
        app.db('employees')
            .where({ id: req.params.id, userId: req.user.id })
            .first()
            .then(employees => {
                if (!employees) {
                    const msg = `Funcionário com id ${req.params.id} não encontrado.`
                    return res.status(400).send(msg)
                }

                const doneAt = employees.doneAt ? null : new Date()
                updateDeliverieDoneAt(req, res, doneAt)
            })
            .cath(err => res.status(400).json(err))
    }

    return { getEmployees, save, remove, toggleEmployees, updateEmployeesDoneAt }
}