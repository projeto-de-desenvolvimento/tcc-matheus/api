const moment = require('moment')

module.exports = app => {
    const getMaintenance = (req, res) => {
        // const date = req.query.date ? req.query.date 
        //     : moment().endOf('day').toDate()
        app.db('maintenances')
            .where({ userId: req.user.id })
            // .where('estimateAt', '<=', date)
            // .orderBy('estimateAt')
            .then(maintenance => { res.json(maintenance) })
            .catch(err => res.status(500).json(err))
    }

    const save = (req, res) => {
        req.body.userId = req.user.id

        app.db('maintenances')
            .insert(req.body)
            .then(_ => res.status(204).send())
            .catch(err => res.status(400).json(err))
    }

    const remove = (req, res) => {
        app.db('maintenances')
            .where({ id: req.params.id, userId: req.user.id })
            .del()
            .then(rowsDeleted => {
                if (rowsDeleted > 0) {
                    res.status(204).send()
                } else {
                    const msg = `Não foi encontrada implemento com id ${req.params.id}`
                    res.status(400).send(msg)
                }
            })
    }

    const updateMaintenanceDoneAt = (req, res, doneAt) => {
        app.db('maintenances')
            .where({ id: req.params.id, userId: req.user.id })
            .update({ doneAt })
            .then(_ => res.status(204).send())
            .cath(err => res.status(400).json(err))
    }

    const toggleMaintenance = (req, res) => {
        app.db('maintenances')
            .where({ id: req.params.id, userId: req.user.id })
            .first()
            .then(maintenance => {
                if (!maintenance) {
                    const msg = `Entrega com id ${req.params.id} não encontrado.`
                    return res.status(400).send(msg)
                }

                const doneAt = maintenance.doneAt ? null : new Date()
                updateDeliverieDoneAt(req, res, doneAt)
            })
            .cath(err => res.status(400).json(err))
    }

    return { getMaintenance, save, remove, toggleMaintenance, updateMaintenanceDoneAt }
}