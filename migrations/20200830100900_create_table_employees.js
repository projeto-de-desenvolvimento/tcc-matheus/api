exports.up = function (knex) {
    return knex.schema.createTable('employees', table => {
        table.increments('id').primary()
        table.string('name').notNull()
        table.string('occupation').notNull()
        table.string('salary').notNull()
        table.datetime('nascimento').notNull().unique()
        table.integer('userId').references('id').inTable('users').notNull()
    })
};

exports.down = function (knex) {
    return knex.schema.dropTableIfExists('employees')
};
