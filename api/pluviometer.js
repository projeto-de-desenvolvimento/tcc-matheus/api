const moment = require('moment')

module.exports = app => {
    const getPluviometers = (req, res) => {

        app.db('pluviometers')
            .where({ userId: req.user.id })
            .orderBy('doneAt')
            .then(pluviometers => { res.json(pluviometers) })
            .catch(err => res.status(500).json(err))
    }

    const save = (req, res) => {
        req.body.userId = req.user.id
        console.log(req.body)

        app.db('pluviometers')
            .insert(req.body)
            .then(_ => res.status(204).send())
            .catch(err => res.status(400).json(err))
    }

    const updatePluviometerDoneAt = (req, res, doneAt) => {
        app.db('pluviometers')
            .where({ id: req.params.id, userId: req.user.id })
            .update({ doneAt })
            .then(_ => res.status(204).send())
            .cath(err => res.status(400).json(err))
    }

    const togglePluviometer = (req, res) => {
        app.db('pluviometers')
            .where({ id: req.params.id, userId: req.user.id })
            .first()
            .then(pluviometer => {
                if (!pluviometers) {
                    const msg = `Chuva com id ${req.params.id} não encontrado.`
                    return res.status(400).send(msg)
                }
                const doneAt = pluviometer.doneAt ? null : new Date()
                updatePluviometerDoneAt(req, res, doneAt)
            })
            .cath(err => res.status(400).json(err))
    }

    return { getPluviometers, save, togglePluviometer, updatePluviometerDoneAt }
}